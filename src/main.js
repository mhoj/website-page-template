import './styles/normalize.css';
import './styles/reset.css';
import './styles/base.css';

import App from './App.svelte';

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
  return parent;
}

const app = new App({
  target: removeAllChildNodes(document.getElementById('content')),
});

export default app;
